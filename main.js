const MODULE_ID = 'lvk-no-jquery-animation'

Hooks.once('init', () => {
    game.settings.register(MODULE_ID, 'disabled', {
        name: `${MODULE_ID}.name`,
        hint: `${MODULE_ID}.hint`,
        scope: 'client',
        config: true,
        type: Boolean,
        default: false,
        onChange: disableAnimations,
    })
})

Hooks.once('ready', () => {
    const disabled = game.settings.get(MODULE_ID, 'disabled')
    disableAnimations(disabled)
})

function disableAnimations(disabled) {
    jQuery.fx.off = disabled
}
